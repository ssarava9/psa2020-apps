#
# Visteon PSA-2020 specific BBCLASS
#

remove_unwanted_service_files () {

	rm -rf ${IMAGE_ROOTFS}/lib/systemd/system/weston.service
}

default_target_ui () {

	rm -rf ${IMAGE_ROOTFS}/etc/systemd/system/default.target
       	ln -sf /lib/systemd/system/ui.target ${IMAGE_ROOTFS}/etc/systemd/system/default.target
}
