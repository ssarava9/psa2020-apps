# Include Base image from BSP
include recipes-core/images/psa2020-dev-image.bb

include psa2020-apps.inc

LICENSE="Visteon"

PV = "1.0"
PR = "r0"

IMAGE_INSTALL_append = " \
    packagegroup-apps \
    "
