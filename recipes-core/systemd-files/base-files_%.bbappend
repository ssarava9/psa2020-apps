PN="base-files"
FILESEXTRAPATHS_append :=":${THISDIR}"

SRC_URI_append = "file://${PN} \
"

do_install_append() {
	install -d ${D}/etc/modules-load.d
	install -d ${D}/etc/modprobe.d

	cp ${WORKDIR}/${PN}/sitronix_i2c_touch.conf	${D}/etc/modules-load.d/
	cp ${WORKDIR}/${PN}/usbmtp.conf	      		${D}/etc/modprobe.d/
}

FILES_${PN} += "/etc \
"
