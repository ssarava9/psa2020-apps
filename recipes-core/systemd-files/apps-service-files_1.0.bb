PN="apps-service-files"
FILESEXTRAPATHS_append :=":${THISDIR}/"

# Copyright Visteon

DESCRIPTION = "Visteon PSA Smartcar 2020 Apps-Services"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${WORKDIR}/${PN}/LICENSE;md5=d41d8cd98f00b204e9800998ecf8427e"

SRC_URI= "file://${PN} \
"

do_install() {
	     install -d ${D}/lib/systemd/system/av.target.wants
             install -d ${D}/lib/systemd/system/ui.target.wants
	     install -d ${D}/lib/systemd/system/early_init.target.wants
	     install -d ${D}/lib/systemd/system/connectivity.target.wants
	     install -d ${D}/lib/systemd/system/multi-user.target.wants

	     cp -rf ${WORKDIR}/${PN}/*target ${D}/lib/systemd/system
	     cp -rf ${WORKDIR}/${PN}/misc/*   ${D}/lib/systemd/system

#	     list_dir=(av ui early_init connectivity)

	     for dir in av ui early_init connectivity
	     do
		for var in $( find ${WORKDIR}/${PN}/$dir -type f)
	    	do
		    file=$(echo $var | awk -F '/' '{print $NF}')
		    cp $var ${D}/lib/systemd/system
		    ln -s /lib/systemd/system/$file ${D}/lib/systemd/system/$dir.target.wants/$file
		    ln -s /lib/systemd/system/$file ${D}/lib/systemd/system/multi-user.target.wants/$file
	        done
	     done
}

FILES_${PN} += "/lib \
		"