DESCRIPTION = "Tata Q5 Apps Component group"
AUTHOR="Sathishmohan S <sathishmohan.saravanan@visteon.com>"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

PACKAGES = "packagegroup-apps"

RDEPENDS_${PN} = "\
	       apps-service-files \
	       linux-imx-soc-headers \
	       common-api-c++ \
	       common-api-c++-dbus \
	       common-api-c++-someip \
	       "
	       